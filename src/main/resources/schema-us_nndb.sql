-- MySQL dump 10.13  Distrib 5.6.10, for osx10.7 (x86_64)
--
-- Host: localhost    Database: nutdat3
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ABBREV`
--

DROP TABLE IF EXISTS `ABBREV`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ABBREV` (
  `NDB_No` varchar(5) DEFAULT NULL,
  `Shrt_Desc` varchar(60) DEFAULT NULL,
  `Water__g_` float(10,2) DEFAULT NULL,
  `Energ_Kcal` int(10) DEFAULT NULL,
  `Protein__g_` float(10,2) DEFAULT NULL,
  `Lipid_Tot__g_` float(10,2) DEFAULT NULL,
  `Ash__g_` float(10,2) DEFAULT NULL,
  `Carbohydrt__g_` float(10,2) DEFAULT NULL,
  `Fiber_TD__g_` float(10,1) DEFAULT NULL,
  `Sugar_Tot__g_` float(10,2) DEFAULT NULL,
  `Calcium__mg_` int(10) DEFAULT NULL,
  `Iron__mg_` float(10,2) DEFAULT NULL,
  `Magnesium__mg_` float DEFAULT NULL,
  `Phosphorus__mg_` int(10) DEFAULT NULL,
  `Potassium__mg_` int(10) DEFAULT NULL,
  `Sodium__mg_` int(10) DEFAULT NULL,
  `Zinc__mg_` float(10,2) DEFAULT NULL,
  `Copper_mg_` float(10,3) DEFAULT NULL,
  `Manganese__mg_` float(10,3) DEFAULT NULL,
  `Selenium__micro_g_` float(10,1) DEFAULT NULL,
  `Vit_C__mg_` float(10,1) DEFAULT NULL,
  `Thiamin__mg_` float(10,3) DEFAULT NULL,
  `Riboflavin__mg_` float(10,3) DEFAULT NULL,
  `Niacin__mg_` float(10,3) DEFAULT NULL,
  `Panto_Acid_mg_` float(10,3) DEFAULT NULL,
  `Vit_B6__mg_` float(10,3) DEFAULT NULL,
  `Folate_Tot__micro_g_` float DEFAULT NULL,
  `Folic_Acid__micro_g_` float DEFAULT NULL,
  `Food_Folate__micro_g_` float DEFAULT NULL,
  `Folate_DFE__micro_g_` float DEFAULT NULL,
  `Choline_Tot__mg_` float DEFAULT NULL,
  `Vit_B12__micro_g_` float(10,2) DEFAULT NULL,
  `Vit_A_IU` int(10) DEFAULT NULL,
  `Vit_A_RAE` float DEFAULT NULL,
  `Retinol__micro_g_` float DEFAULT NULL,
  `Alpha_Carot__micro_g_` float DEFAULT NULL,
  `Beta_Carot__micro_g_` float DEFAULT NULL,
  `Beta_Crypt__micro_g_` float DEFAULT NULL,
  `Lycopene__micro_g_` float DEFAULT NULL,
  `Lut_Zea__micro__g_` float DEFAULT NULL,
  `Vit_E__mg_` float(10,2) DEFAULT NULL,
  `Vit_D_micro_g_` float(10,1) DEFAULT NULL,
  `Vit_D_IU` float DEFAULT NULL,
  `Vit_K__micro_g_` float(10,1) DEFAULT NULL,
  `FA_Sat__g_` float(10,3) DEFAULT NULL,
  `FA_Mono__g_` float(10,3) DEFAULT NULL,
  `FA_Poly__g_` float(10,3) DEFAULT NULL,
  `Cholestrl__mg_` int(10) DEFAULT NULL,
  `GmWt_1` float(9,2) DEFAULT NULL,
  `GmWt_Desc1` varchar(120) DEFAULT NULL,
  `GmWt_2` float(9,2) DEFAULT NULL,
  `GmWt_Desc2` varchar(120) DEFAULT NULL,
  `Refuse_Pct` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ADD_FTNT`
--

DROP TABLE IF EXISTS `ADD_FTNT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ADD_FTNT` (
  `NDB_NO` varchar(255) DEFAULT NULL,
  `Footnt_No` varchar(255) DEFAULT NULL,
  `Footnot_Typ` varchar(255) DEFAULT NULL,
  `Nutr_No` varchar(255) DEFAULT NULL,
  `Footnot_Txt` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DATA_SRC`
--

DROP TABLE IF EXISTS `DATA_SRC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DATA_SRC` (
  `DataSrc_ID` varchar(6) NOT NULL,
  `Authors` varchar(255) DEFAULT NULL,
  `Title` varchar(255) NOT NULL,
  `Year` varchar(4) DEFAULT NULL,
  `Journal` varchar(135) DEFAULT NULL,
  `Vol_City` varchar(16) DEFAULT NULL,
  `Issue_State` varchar(5) DEFAULT NULL,
  `Start_Page` varchar(5) DEFAULT NULL,
  `End_Page` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`DataSrc_ID`),
  CONSTRAINT `fk_DATA_SRC_DATSRCLN` FOREIGN KEY (`DataSrc_ID`) REFERENCES `DATSRCLN` (`DataSrc_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DATSRCLN`
--

DROP TABLE IF EXISTS `DATSRCLN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DATSRCLN` (
  `NDB_No` varchar(5) NOT NULL,
  `Nutr_No` varchar(3) NOT NULL,
  `DataSrc_ID` varchar(6) NOT NULL,
  PRIMARY KEY (`NDB_No`,`Nutr_No`,`DataSrc_ID`),
  KEY `fk_DATSRCLN_NUT_DAT_idx` (`NDB_No`,`Nutr_No`),
  KEY `fk_DATSRCLN_DATASRC_idx` (`DataSrc_ID`),
  CONSTRAINT `fk_DATSRCLN_DATASRC` FOREIGN KEY (`DataSrc_ID`) REFERENCES `DATA_SRC` (`DataSrc_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_DATSRCLN_NUT_DAT1` FOREIGN KEY (`NDB_No`, `Nutr_No`) REFERENCES `NUT_DATA` (`NDB_No`, `Nutr_No`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DERIV_CD`
--

DROP TABLE IF EXISTS `DERIV_CD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DERIV_CD` (
  `Deriv_CD` varchar(4) NOT NULL,
  `Deriv_Desc` varchar(120) NOT NULL,
  PRIMARY KEY (`Deriv_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FD_GROUP`
--

DROP TABLE IF EXISTS `FD_GROUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FD_GROUP` (
  `FdGrp_CD` varchar(4) NOT NULL,
  `FdGrp_Desc` varchar(60) NOT NULL,
  PRIMARY KEY (`FdGrp_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FOOD_DES`
--

DROP TABLE IF EXISTS `FOOD_DES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FOOD_DES` (
  `NDB_No` varchar(5) NOT NULL,
  `FdGrp_Cd` varchar(4) NOT NULL,
  `Long_Desc` varchar(200) NOT NULL,
  `Shrt_Desc` varchar(60) NOT NULL,
  `ComName` varchar(100) DEFAULT NULL,
  `ManufacName` varchar(65) DEFAULT NULL,
  `Survey` varchar(1) DEFAULT NULL,
  `Ref_Desc` varchar(135) DEFAULT NULL,
  `Refuse` int(2) DEFAULT NULL,
  `SciName` varchar(65) DEFAULT NULL,
  `N_Factor` float(4,2) DEFAULT NULL,
  `Pro_Factor` float(4,2) DEFAULT NULL,
  `Fat_Factor` float(4,2) DEFAULT NULL,
  `CHO_Factor` float(4,2) DEFAULT NULL,
  PRIMARY KEY (`NDB_No`),
  KEY `fk_food_group` (`FdGrp_Cd`),
  CONSTRAINT `fk_food_group` FOREIGN KEY (`FdGrp_Cd`) REFERENCES `FD_GROUP` (`FdGrp_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FOOTNOTE`
--

DROP TABLE IF EXISTS `FOOTNOTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FOOTNOTE` (
  `NDB_No` varchar(5) NOT NULL,
  `Footnt_No` varchar(4) NOT NULL,
  `Footnot_Typ` varchar(1) NOT NULL,
  `Nutr_No` varchar(3) DEFAULT NULL,
  `Footnot_Txt` varchar(200) NOT NULL,
  KEY `fk_FOOTNOTE_FOOD_DES_idx` (`NDB_No`),
  CONSTRAINT `fk_FOOTNOTE_FOOD_DES` FOREIGN KEY (`NDB_No`) REFERENCES `FOOD_DES` (`NDB_No`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LANGDESC`
--

DROP TABLE IF EXISTS `LANGDESC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LANGDESC` (
  `Factor_Code` varchar(5) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`Factor_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LANGUAL`
--

DROP TABLE IF EXISTS `LANGUAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LANGUAL` (
  `NDB_No` varchar(5) NOT NULL,
  `Factor_Code` varchar(5) NOT NULL,
  PRIMARY KEY (`NDB_No`,`Factor_Code`),
  KEY `FK_LANGUAL_FOOD_DES_idx` (`NDB_No`),
  KEY `fk_LANGUAL_FACTOR_idx` (`Factor_Code`),
  CONSTRAINT `fk_LANGUAL_FACTOR` FOREIGN KEY (`Factor_Code`) REFERENCES `LANGDESC` (`Factor_Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_LANGUAL_LANGDESC` FOREIGN KEY (`NDB_No`) REFERENCES `FOOD_DES` (`NDB_No`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NUTR_DEF`
--

DROP TABLE IF EXISTS `NUTR_DEF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NUTR_DEF` (
  `Nutr_No` varchar(3) NOT NULL,
  `Units` varchar(7) NOT NULL,
  `Tagname` varchar(20) DEFAULT NULL,
  `NutrDesc` varchar(60) NOT NULL,
  `Num_Dec` varchar(1) NOT NULL,
  `SR_Order` float NOT NULL,
  PRIMARY KEY (`Nutr_No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NUT_DATA`
--

DROP TABLE IF EXISTS `NUT_DATA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NUT_DATA` (
  `NDB_No` varchar(5) NOT NULL,
  `Nutr_No` varchar(3) NOT NULL,
  `Nutr_Val` float(10,3) NOT NULL,
  `Num_Data_Pts` int(5) NOT NULL,
  `Std_Error` float(8,3) DEFAULT NULL,
  `Src_Cd` varchar(2) NOT NULL,
  `Deriv_Cd` varchar(4) DEFAULT NULL,
  `Ref_NDB_No` varchar(5) DEFAULT NULL,
  `Add_Nutr_Mark` varchar(1) DEFAULT NULL,
  `Num_Studies` int(2) DEFAULT NULL,
  `Min` float(10,3) DEFAULT NULL,
  `Max` float(10,3) DEFAULT NULL,
  `DF` float DEFAULT NULL,
  `Low_EB` float(10,3) DEFAULT NULL,
  `Up_EB` float(10,3) DEFAULT NULL,
  `Stat_Cmt` varchar(10) DEFAULT NULL,
  `AddMod_Date` varchar(10) DEFAULT NULL,
  `NUT_DATAcol` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`NDB_No`,`Nutr_No`),
  KEY `fk_NUT_DATA_FOOD_DES1_idx` (`NDB_No`),
  KEY `fk_NUT_DATA_NUT_DEF_idx` (`Nutr_No`),
  KEY `fk_NUT_DATA_DERIV_CODE_idx` (`Deriv_Cd`),
  KEY `fk_NUT_DATA_SRC_CODE_idx` (`Src_Cd`),
  KEY `fk_NUT_DATA_FOOD_DES2_idx` (`Ref_NDB_No`),
  CONSTRAINT `fk_NUT_DATA_DERIV_CODE` FOREIGN KEY (`Deriv_Cd`) REFERENCES `DERIV_CD` (`Deriv_CD`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_NUT_DATA_FOOD_DES1` FOREIGN KEY (`NDB_No`) REFERENCES `FOOD_DES` (`NDB_No`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_NUT_DATA_FOOD_DES2` FOREIGN KEY (`Ref_NDB_No`) REFERENCES `FOOD_DES` (`NDB_No`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_NUT_DATA_NUT_DEF` FOREIGN KEY (`Nutr_No`) REFERENCES `NUTR_DEF` (`Nutr_No`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_NUT_DATA_SRC_CODE` FOREIGN KEY (`Src_Cd`) REFERENCES `SRC_CD` (`Src_Cd`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SRC_CD`
--

DROP TABLE IF EXISTS `SRC_CD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SRC_CD` (
  `Src_Cd` varchar(2) NOT NULL,
  `SrcCd_Desc` varchar(60) NOT NULL,
  PRIMARY KEY (`Src_Cd`),
  CONSTRAINT `fk_SRC_CD_NUT_DAT` FOREIGN KEY (`Src_Cd`) REFERENCES `NUT_DATA` (`Src_Cd`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WEIGHT`
--

DROP TABLE IF EXISTS `WEIGHT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WEIGHT` (
  `NDB_No` varchar(5) NOT NULL,
  `Seq` int(2) NOT NULL AUTO_INCREMENT,
  `Amount` float(5,3) NOT NULL,
  `Msre_Desc` varchar(84) NOT NULL,
  `Gm_Wgt` float(7,1) NOT NULL,
  `Num_Data_Pts` int(3) DEFAULT NULL,
  `Std_Dev` float(7,3) DEFAULT NULL,
  PRIMARY KEY (`Seq`,`NDB_No`),
  KEY `fk_WEIGHT_NUT_DATA_idx` (`NDB_No`),
  CONSTRAINT `fk_WEIGHT_NUT_DATA` FOREIGN KEY (`NDB_No`) REFERENCES `FOOD_DES` (`NDB_No`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  3:37:26
