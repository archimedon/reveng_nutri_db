package net.secretriver.sqlconv

import scala.collection.mutable._

class Ident {

  var ver = -1
	var text: String = "Null"

  /**
	 * Transform the text and returns self, `this`
	 */
  def transform(f: (String)=>String ) = {
	 setText(f(text))
	}

  /**
	 * Sets the text then returns `this`
	 */
	def setText(txt: String) : Ident = {
	  if ( ! this.text.equals(txt)) {
	    ver += 1
  		text = txt;
	  }
	  this;
  }

	def getText = text
	def getVer = ver
	def isUnModified = ver == 0

	override def toString = text
	
 	def intrinsicVal(that: Ident): Boolean = {
	  this.getText.equals(that.getText)
	}
	
	override def equals(that: Any): Boolean = {
	  that match {
      case that:Ident   => this.getText.equals(that.getText)
      case that:String  => this.getText.equals(that)
      case that         => this.getText.equals(that.toString)
    }
	}

  
  def equalsIgnoreCase(that: Any): Boolean = {
	  that match {
	  case that:Ident   => this.getText.equalsIgnoreCase(that.getText)
	  case that:String  => this.getText.equalsIgnoreCase(that)
	  case that         => this.getText.equalsIgnoreCase(that.toString)
	  }
  }
  
	override def hashCode(): Int = this.getText.hashCode

}

object Ident {

	def apply(txt: String) = {
	  (new Ident).setText(txt)
	}

}