package net.secretriver.sqlconv

import scala.collection.mutable._


class Constraint(idxLabel: String, referencedTable: Table, refMap: scala.collection.immutable.Map[Ident, Ident], actions: String ) {
  var label = idxLabel;
  var idmod = 0
  
	def getLabel: String = label
	def getReferencedIdent: Ident = referencedTable.getTableIdent
	
	def getRefMap: scala.collection.immutable.Map[Ident, Ident] = refMap
	def getactions: String = actions

//	// Methods
	def setLabel(nuLabel: String) : Constraint = { label = nuLabel; this }
	
	def transformReferenceIdent( f: (String)=>(String)) = {
	  idmod += 1
	  referencedTable.getTableIdent.transform(f)
	}

	override def toString = {
    val keys = getRefMap.keySet.map { x => "`" + x.getText + "`" }.mkString(", ")
    val vals = getRefMap.values.map { x => "`" + x.getText + "`" }.mkString(", ")
    s"""CONSTRAINT FOREIGN KEY ($keys) REFERENCES `${referencedTable.getTableIdent}` ($vals) $actions"""
  }    
}