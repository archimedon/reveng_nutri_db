package net.secretriver.sqlconv
import scala.io.Source
import scala.collection.mutable.{Map, LinkedHashSet,LinkedHashMap,MutableList}

object AlterSchema {
  /**
   * Initial SQL directives; "Relax constraints"
   */
  val preambleSql = """/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;"""

  /**
   * Revert SQL integrity checks
   */
  val revertSql = """
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;"""

  def main(args: Array[String]): Unit = {
    
		val tableMap = parseFile(args(0))
		val patMap = loadPatterns(args(1))
		
    println(preambleSql)
    printTables(tableMap, patMap)
    println(revertSql)
	}
  
  def printTables(tableMap: Map[Ident, Table], patMap: Map[Ident, List[String]]) = {
		tableMap.values.toList
		.map { renamePrimary }
//    .sortBy { _.numPks}
    .sortBy { _.constraints.size }
    .map {  table =>
      patMap.get(table.getTableIdent) match {
        case Some(ary) => {
          ary.foreach {
            tnamePref => {
              table.refactorIdent(tnamePref)
            }
          }
        }
        case None => 
      }
      table
    }
//    .map { makePatternProperty }
    .map(rebalanceRefs)
    .map(loweCaseIdents)
    .foreach(println)
  }	
  def loweCaseIdents(table: Table) = {
	  table.refactorIdent(table.getName.toLowerCase())
    table.columns.foreach { col => col.getIdent.setText(col.getIdent.getText.toLowerCase()) }
	  table
  }
	
  def renamePrimary(table: Table) = {
    if (table.numPks == 1) {
      table.columns.find { _.isPrimaryKey }
      match {
        case Some(col) => col.getIdent.setText("ID")
        case None =>
      }
    }
    table
  }
  // The following 2 functions could be more efficient.
  def mkuniq(string: String, table: Table, ident : Ident) : String = {
    val regex1 = (string + """(\d*)$""").r
    val num = table.columns.filterNot(_.getIdent == ident).filter { _.getIdent.getText match {
      case regex1(n) => true 
      case _ => false
    } }.size
    string + ( if (num == 0) "" else num)
  }
  
  def rebalanceRefs(table: Table) = {
	  table.constraints.foreach( constraint => {
		  constraint.getRefMap.filter(c2rp => c2rp._2.equalsIgnoreCase("ID")).foreach( c2rp => {
			  c2rp._1.setText(mkuniq(constraint.getReferencedIdent.getText + "_ID", table, c2rp._1))
		  })
	  })
	  table
  }

  /**
   * Load table-name variants from file
   */
  def loadPatterns(fpath: String) = {
    import scala.io.Source.fromURL
    import scala.collection.JavaConverters._    
    val reader = Source.fromFile(fpath).bufferedReader()
    val prop = new java.util.Properties()
    prop.load(reader)
    reader.close()
    prop.asScala.map{
      xtup => Table.identFactory.getIdent(xtup._1) -> xtup._2.split(", ").toList
    }
  }
  
  def makePatternProperty(table: Table) = {

    val tname = table.getName
    val pattset = new MutableList[String]()
    pattset += tname
     
    if (tname.contains("_")) pattset += (tname.replaceAll("_", ""))
    if (tname.contains("_CD")) pattset += (tname.replaceAll("_CD", ""))
     
    s"""${tname}=""" + pattset.mkString(", ")
    
  }
  
	def parseFile(filepath: String) : Map[Ident, Table] = {

		var tabOut: Table = null
		
		val source = Source.fromFile(filepath)
		
	  for (line <- source.getLines()) line.trim match {

			case MySQLSchemaPatterns.createTblRegex(tablename) => {
			  tabOut = Table.getOrCreate(tablename)
			}
			/**
			 * Matches each table's columns
			 */
			case MySQLSchemaPatterns.colDefRegex(colName, fieldDef) => {
				// todo check if exists 
				tabOut.addColumn(colName, fieldDef)
			}
			/**
			 * Matches: PRIMARY KEY (`Seq`,`NDB_No`)
			 */
			case MySQLSchemaPatterns.pkRegex(keysStr) => {
				keysStr.replaceAll("`", "")
				.split(""",\s*""")
				.foreach { colName => tabOut.setPrimaryKey(colName)  }
			}
			/**
			 * CONSTRAINT `fk_ DATSRCLN_NUT_DAT1` FOREIGN KEY (`NDB_No`, `Nutr_No`)
			 * REFERENCES `NUT_DATA` (`NDB_No`, `Nutr_No`) ON DELETE NO ACTION ON UPDATE NO ACTION
			 */
			case MySQLSchemaPatterns.constraintsRegex(idxLabel, foreignKeys, referencedTable,
		  	keyReferences, actions ) => {
				val fKeys = foreignKeys.replaceAll("`", "").split(", ")
				val keyRefs = keyReferences.replaceAll("`", "").split(", ")
				tabOut.addConstraints(idxLabel, referencedTable, fKeys, keyRefs, actions.replace(",", "") )
	  	}
      case MySQLSchemaPatterns.endDescRegex(enginedesc) => {
        //println("columns:\n" + tabOut.columns.mkString("\n"))
    	  //println("<END>" + enginedesc + "</END>")
      } 
		  case dump => {
        //println("<DUMP>" + dump + "</DUMP>")
      }
		}
		source.close()
		Table.tables
	}
}
