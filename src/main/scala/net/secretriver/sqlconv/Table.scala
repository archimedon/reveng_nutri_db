package net.secretriver.sqlconv

import scala.io.Source._
import scala.collection.mutable.{Set, Map, LinkedHashSet,HashMap,LinkedHashMap,MutableList}

class Table(val tableIdent: Ident) {

	Table.tables.put(tableIdent, this)
  
	val columnIdentFactory = new IdentFactory
	
	val primaryKeys = new MutableList[Ident]()
	val columns = new LinkedHashSet[Column]()
	val constraints = LinkedHashSet[Constraint]()
	
  val dependents = new HashMap[Ident, Array[Ident]]

	var numPks = 0
	
  def getTableIdent : Ident = tableIdent
  
  def getName : String = tableIdent.getText
  
  def hasSinglePK : Boolean = numPks == 1
  
	def setName(tablename: String) : Table = {
    tableIdent.setText(tablename);
    this
  }

	def getColumns = columns
	
  val tn = getName
	val tn_nosep = tn.replaceAll("_", "")
	
	def pushDependent(ident: Ident, keys: Array[Ident]) = {
	  dependents+=(ident -> keys)
	}
	
	def colRefTabPatternStr(numSeps: Int) : String = s"""(?i)(${tn}|${tn_nosep})_(.+)""" + "\\s*$"
  val tableNamePattern = colRefTabPatternStr(2)
  
  
  def refactorIdent(tnamePref: String) = {
		val currName = getName
	  
    val headerPattern1 = (s"""(?i)^${currName}_{0,1}(.+)""" + "$").r
		val headerPattern2 = (s"""(?i)^${tnamePref}_{0,1}(.+)""" + "$").r

    /**
     * Step  through the columns
     */
    columns.filter(_.isEditable).foreach ( col => {
      val col_name = col.getIdent.getText.trim()
      
      col.getIdent.setText(
        if (col.isPrimaryKey && numPks == 1) {
          "ID"
        }
        else if (currName.equalsIgnoreCase(col_name) || tnamePref.equalsIgnoreCase(col_name) ) {
            // then this is likely the PK
           "ID"
        }
        else {
          col_name match {
            case headerPattern1(linkedName)  => linkedName
            case headerPattern2(linkedName)  => linkedName
            // It's one of several primary keys
            case asis =>   asis
          }
        }
      )
    })
    
    dependents.foreach(depEntry => depEntry._2.foreach {
      remIdent => {
        val remCol = remIdent.getText.trim()
        remIdent.setText(
          if (currName.equalsIgnoreCase(remCol)) {
             s"""${tnamePref}_ID"""
          }
          else {
            remCol match {
               case  headerPattern1(prop) => {
                  if ("CD_ID".equalsIgnoreCase(prop)) {
                    s"""${tnamePref}_ID"""
                  }
                  else {                    
                    s"""${tnamePref}_${prop}"""
                  }
               }
               case  headerPattern2(prop) => {
            	   if ("CD_ID".equalsIgnoreCase(prop)) {
            		   s"""${tnamePref}_ID"""
            	   }
            	   else {                    
            		    s"""${tnamePref}_${prop}"""
            	   }
               }
               case d => {
                 d
               }
            }
          }
        )
      }
    })
    
    tableIdent.setText(tnamePref);
	}
	
	// todo check if exists 
	def addConstraints(
	    idxLabel: String,
      referencedTableName: String,
      fKeys: Array[String],
      keyRefs: Array[String],
      actions: String ) = {
	  
    val referencedTable = Table.getOrCreate(referencedTableName)
    val fkIdents = fKeys.map { x => columnIdentFactory.getIdent(x) }
  	val remIdents = keyRefs.map { x => referencedTable.columnIdentFactory.getIdent(x) }
  	
    fkIdents.foreach {
      ident => 
        {
        val fnd_col = columns.find { col => col.getIdent.equals(ident) } 
        fnd_col  match {
        case Some(fnd) => {
          fnd.setForeignKey(true)
        }
        case None      =>   {}
        }
      }
	  }
    referencedTable.pushDependent(this.getTableIdent, fkIdents)

	  constraints += new Constraint(
      idxLabel,
      referencedTable,
      (fkIdents zip remIdents ).toMap,
      actions
    );

	  this
	}
	
	// todo check if exists 
	def addColumn(colName: String, fieldDef: String) : Table = {
		columns.add(new Column(columnIdentFactory.getIdent(colName), fieldDef.replaceAll(""",\s*$""", ""), this))
		this
	}

	def findColumn(colName: Any) = columns.find { col => col.getIdent.equals(colName) }
	
	// PRIMARY KEY (`Seq`,`NDB_No`),
	def setPrimaryKey(colName: String) = {
	  findColumn(colName) match {
	    case Some(column) => {
	      numPks += 1
	      column.setPrimaryKey(true)
	      primaryKeys += column.getIdent
	    }
	    case _ =>	{ throw new Exception("Indicated Column does not exist.") }
	  }
		this
	}

	def setPrimaryKeys(colNames: String*) = {
    colNames.foreach(setPrimaryKey)
    this
	}

	def pkSql = {
    if (primaryKeys.isEmpty) {
      ""
    }else {
      "PRIMARY KEY (" + primaryKeys.map("`" + _.getText + "`").mkString(", ") + ")"
    }
  }
   	
	override def toString = {
	  val constraintsSep = if (constraints.isEmpty) "" else "\n"
	  val columnSep = if (primaryKeys.nonEmpty || constraints.nonEmpty) ",\n" else ""
	  val pkSep = if (primaryKeys.nonEmpty && constraints.nonEmpty) ",\n" else "\n"
	  s"""
  	|CREATE TABLE `${tableIdent.getText}` (
  	|  ${columns.mkString(",\n  ")}${columnSep}${pkSql}${pkSep}${constraints.mkString(",\n  ")}${constraintsSep}) ENGINE=InnoDB DEFAULT CHARSET=utf8;""".stripMargin
	}
	
}// End class

object Table {

	val tables = new LinkedHashMap[Ident, Table]()
	val identFactory = new IdentFactory

	/**
	 * Prevents duplicates in the set of tables.
	 * Returns a Table, a new one if none exists with the same 'name'
	 */
	def getOrCreate(atref: Any) : Table = {
    	  
	  val tableref = atref match {
	    case ref: Table => ref.getTableIdent
	    case ref: Ident => ref
	    case ref: String => identFactory.getIdent(ref)
	    case ref  => identFactory.getIdent(ref.toString)
	  }
	  
  	tables.get(tableref) match {
			case Some(table)  => {
			  table
			}
			case None	        => {
    		new Table(tableref)
    	}
   	}
	}

	def exists(tablename: Any) : Boolean = {
	  tables.keySet.exists { x => x.equals(tablename) }
	}

	def find(tablename: Ident) : Option[Table] = {
	  tables.get(tablename)
	}
	
}

