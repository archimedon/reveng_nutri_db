package net.secretriver.sqlconv

import scala.collection.mutable.{ LinkedHashSet, HashMap }

class Column(columnIdent: Ident, definition: String, table: Table) {

  var primaryKey: Boolean = false
  var foreignKey: Boolean = false
  var editable: Boolean = true

  override def equals(colNmOrRef: Any): Boolean = {
    colNmOrRef match {
      case that: Column => { columnIdent.equals(that.getText) }
      case that: Ident  => { columnIdent.equals(that) }
      case that: String => { columnIdent.equals(that) }
      case _            => { false }
    }
  }

  def getText: String = columnIdent.getText

  def getIdent: Ident = columnIdent
  
  def getTable: Table = table

  def isPrimaryKey: Boolean = primaryKey
  def setPrimaryKey(stat: Boolean) = { primaryKey = stat }

  def isForeignKey: Boolean = foreignKey
  def setForeignKey(stat: Boolean) = { foreignKey = stat }

  def isEditable: Boolean = editable
  def setEditable(stat: Boolean) = { editable = stat }

  def refactorName(nuName: String) = {
    columnIdent.setText(nuName)
  }
  
  override def toString: String = { 
    s"`$columnIdent` $definition"
  }
}
