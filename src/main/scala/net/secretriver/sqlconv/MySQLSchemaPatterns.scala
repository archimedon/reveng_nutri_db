package net.secretriver.sqlconv

object MySQLSchemaPatterns {
	val plainLabel = """\`[^\`]+\`"""
	val captureLabel = """\`([^\`]+)\`"""

	val tableSep = """\n--\n-- Table structure for table\s*""" + plainLabel + """\s*\n--"""

	val createTblPattern = """CREATE TABLE +""" + captureLabel + """ +\("""
	val createTblRegex = createTblPattern.r

	// Get column definition
	val colNamePattern = captureLabel

	val dataDefPattern = """(.+)(?:,){0,1}$"""
	val dataDefRegex = dataDefPattern.r

	val colDefPattern = colNamePattern + """\s+""" + dataDefPattern
	
	/** Matches SQL column names and the data definition. 
	 *  Ex: `Title` varchar(255) NOT NULL,
	 */
	val colDefRegex = colDefPattern.r

	// PRIMARY KEY (`Seq`,`NDB_No`),
	val pkPattern = """PRIMARY KEY\s+\(([^\)]+)\).*$"""
	val pkRegex = pkPattern.r

	//   KEY `fk_ DATSRCLN_NUT_DAT_idx` (`NDB_No`,`Nutr_No`),
	val keyRefPattern = """  KEY """ + captureLabel + """  \(([^\)]+)\),{0,1}$"""
	val keyRefRegex = keyRefPattern.r


	// CONSTRAINT `fk_LANGUAL_FACTOR` FOREIGN KEY (`Factor_Code`) 
	// REFERENCES `LANGDESC` (`Factor_Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,

	// CONSTRAINT `fk_ DATSRCLN_NUT_DAT1` FOREIGN KEY (`NDB_No`, `Nutr_No`)
	// REFERENCES `NUT_DATA` (`NDB_No`, `Nutr_No`) ON DELETE NO ACTION ON UPDATE NO ACTION
	val constraintsPattern = "CONSTRAINT " + captureLabel + """ FOREIGN KEY \(([^\)]+)\)""" + " REFERENCES " + captureLabel + """ \(([^\)]+)\)(.*)$""" 

	val constraintsRegex = constraintsPattern.r

	val endDescRegex = """\) (ENGINE=[\w\_\-]+).+$""".r
}