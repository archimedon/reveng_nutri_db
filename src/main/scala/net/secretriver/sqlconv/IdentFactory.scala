package net.secretriver.sqlconv

import scala.collection.mutable.LinkedHashSet

class IdentFactory {

	val idents = new LinkedHashSet[Ident]()

	def getIdents = idents
	
	def getIdent(txt: String): Ident = {
  	idents.find(_.equals(txt)) match {
			case Some(ident) => ident
			case None	=> {
        val newIdent = Ident(txt)
        idents += newIdent
        newIdent
      }
  	}
	}
	
}
