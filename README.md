# Sanitize US National Nutritional DB #

As a first step, convert column names used in the NDB to names that can be cast/generated into coded object properties.

### The Scala script does the following: ###

  - First, primary-keys are identified (where possible) and renamed to ID
  - Then, using the name variant provided in `src/main/resources/name_conversion.properties` tables and related columns are matched and renamed... with the eventual table name being the name, last in the array, provided in the properties file
  - A few other alterations occur so foreign keys are named like foreign keys
  - Finally, all name are lowercased

### How to execute ###

* Clone this repository

```shell
$ scalac -classpath src/main/scala -d ./bin src/main/scala/net/secretriver/sqlconv/*

$ scala -classpath ./bin net.secretriver.sqlconv.AlterSchema src/main/resources/schema-us_nndb.sql src/main/resources/name_conversion.properties  > ./output/clean_schema-us_nndb.sql
```

### Output ###

The cleaned schema will be in `./output/clean_schema-us_nndb.sql`