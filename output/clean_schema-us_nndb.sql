/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

CREATE TABLE `abbrev` (
  `ndb_no` varchar(5) DEFAULT NULL,
  `shrt_desc` varchar(60) DEFAULT NULL,
  `water__g_` float(10,2) DEFAULT NULL,
  `energ_kcal` int(10) DEFAULT NULL,
  `protein__g_` float(10,2) DEFAULT NULL,
  `lipid_tot__g_` float(10,2) DEFAULT NULL,
  `ash__g_` float(10,2) DEFAULT NULL,
  `carbohydrt__g_` float(10,2) DEFAULT NULL,
  `fiber_td__g_` float(10,1) DEFAULT NULL,
  `sugar_tot__g_` float(10,2) DEFAULT NULL,
  `calcium__mg_` int(10) DEFAULT NULL,
  `iron__mg_` float(10,2) DEFAULT NULL,
  `magnesium__mg_` float DEFAULT NULL,
  `phosphorus__mg_` int(10) DEFAULT NULL,
  `potassium__mg_` int(10) DEFAULT NULL,
  `sodium__mg_` int(10) DEFAULT NULL,
  `zinc__mg_` float(10,2) DEFAULT NULL,
  `copper_mg_` float(10,3) DEFAULT NULL,
  `manganese__mg_` float(10,3) DEFAULT NULL,
  `selenium__micro_g_` float(10,1) DEFAULT NULL,
  `vit_c__mg_` float(10,1) DEFAULT NULL,
  `thiamin__mg_` float(10,3) DEFAULT NULL,
  `riboflavin__mg_` float(10,3) DEFAULT NULL,
  `niacin__mg_` float(10,3) DEFAULT NULL,
  `panto_acid_mg_` float(10,3) DEFAULT NULL,
  `vit_b6__mg_` float(10,3) DEFAULT NULL,
  `folate_tot__micro_g_` float DEFAULT NULL,
  `folic_acid__micro_g_` float DEFAULT NULL,
  `food_folate__micro_g_` float DEFAULT NULL,
  `folate_dfe__micro_g_` float DEFAULT NULL,
  `choline_tot__mg_` float DEFAULT NULL,
  `vit_b12__micro_g_` float(10,2) DEFAULT NULL,
  `vit_a_iu` int(10) DEFAULT NULL,
  `vit_a_rae` float DEFAULT NULL,
  `retinol__micro_g_` float DEFAULT NULL,
  `alpha_carot__micro_g_` float DEFAULT NULL,
  `beta_carot__micro_g_` float DEFAULT NULL,
  `beta_crypt__micro_g_` float DEFAULT NULL,
  `lycopene__micro_g_` float DEFAULT NULL,
  `lut_zea__micro__g_` float DEFAULT NULL,
  `vit_e__mg_` float(10,2) DEFAULT NULL,
  `vit_d_micro_g_` float(10,1) DEFAULT NULL,
  `vit_d_iu` float DEFAULT NULL,
  `vit_k__micro_g_` float(10,1) DEFAULT NULL,
  `fa_sat__g_` float(10,3) DEFAULT NULL,
  `fa_mono__g_` float(10,3) DEFAULT NULL,
  `fa_poly__g_` float(10,3) DEFAULT NULL,
  `cholestrl__mg_` int(10) DEFAULT NULL,
  `gmwt_1` float(9,2) DEFAULT NULL,
  `gmwt_desc1` varchar(120) DEFAULT NULL,
  `gmwt_2` float(9,2) DEFAULT NULL,
  `gmwt_desc2` varchar(120) DEFAULT NULL,
  `refuse_pct` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `addftnt` (
  `ndb_no` varchar(255) DEFAULT NULL,
  `footnt_no` varchar(255) DEFAULT NULL,
  `footnot_typ` varchar(255) DEFAULT NULL,
  `nutr_no` varchar(255) DEFAULT NULL,
  `footnot_txt` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `deriv` (
  `id` varchar(4) NOT NULL,
  `desc` varchar(120) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fdgroup` (
  `id` varchar(4) NOT NULL,
  `desc` varchar(60) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `langdesc` (
  `id` varchar(5) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `nutrdef` (
  `id` varchar(3) NOT NULL,
  `units` varchar(7) NOT NULL,
  `tagname` varchar(20) DEFAULT NULL,
  `nutrdesc` varchar(60) NOT NULL,
  `num_dec` varchar(1) NOT NULL,
  `sr_order` float NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `datasrc` (
  `id` varchar(6) NOT NULL,
  `authors` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `year` varchar(4) DEFAULT NULL,
  `journal` varchar(135) DEFAULT NULL,
  `vol_city` varchar(16) DEFAULT NULL,
  `issue_state` varchar(5) DEFAULT NULL,
  `start_page` varchar(5) DEFAULT NULL,
  `end_page` varchar(5) DEFAULT NULL,
PRIMARY KEY (`id`),
CONSTRAINT FOREIGN KEY (`id`) REFERENCES `datsrcln` (`datasrc_id`)  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fooddes` (
  `id` varchar(5) NOT NULL,
  `fdgroup_id` varchar(4) NOT NULL,
  `long_desc` varchar(200) NOT NULL,
  `shrt_desc` varchar(60) NOT NULL,
  `comname` varchar(100) DEFAULT NULL,
  `manufacname` varchar(65) DEFAULT NULL,
  `survey` varchar(1) DEFAULT NULL,
  `ref_desc` varchar(135) DEFAULT NULL,
  `refuse` int(2) DEFAULT NULL,
  `sciname` varchar(65) DEFAULT NULL,
  `n_factor` float(4,2) DEFAULT NULL,
  `pro_factor` float(4,2) DEFAULT NULL,
  `fat_factor` float(4,2) DEFAULT NULL,
  `cho_factor` float(4,2) DEFAULT NULL,
PRIMARY KEY (`id`),
CONSTRAINT FOREIGN KEY (`fdgroup_id`) REFERENCES `fdgroup` (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `footnote` (
  `fooddes_id` varchar(5) NOT NULL,
  `no` varchar(4) NOT NULL,
  `typ` varchar(1) NOT NULL,
  `nutr_no` varchar(3) DEFAULT NULL,
  `txt` varchar(200) NOT NULL,

CONSTRAINT FOREIGN KEY (`fooddes_id`) REFERENCES `fooddes` (`id`)  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `src` (
  `id` varchar(2) NOT NULL,
  `desc` varchar(60) NOT NULL,
PRIMARY KEY (`id`),
CONSTRAINT FOREIGN KEY (`id`) REFERENCES `nutdata` (`src_id`)  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `weight` (
  `fooddes_id` varchar(5) NOT NULL,
  `seq` int(2) NOT NULL AUTO_INCREMENT,
  `amount` float(5,3) NOT NULL,
  `msre_desc` varchar(84) NOT NULL,
  `gm_wgt` float(7,1) NOT NULL,
  `num_data_pts` int(3) DEFAULT NULL,
  `std_dev` float(7,3) DEFAULT NULL,
PRIMARY KEY (`seq`, `fooddes_id`),
CONSTRAINT FOREIGN KEY (`fooddes_id`) REFERENCES `fooddes` (`id`)  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `datsrcln` (
  `ndb_no` varchar(5) NOT NULL,
  `nutr_no` varchar(3) NOT NULL,
  `datasrc_id` varchar(6) NOT NULL,
PRIMARY KEY (`ndb_no`, `nutr_no`, `datasrc_id`),
CONSTRAINT FOREIGN KEY (`datasrc_id`) REFERENCES `datasrc` (`id`)  ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FOREIGN KEY (`ndb_no`, `nutr_no`) REFERENCES `nutdata` (`fooddes_id`, `nutrdef_id`)  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `langual` (
  `fooddes_id` varchar(5) NOT NULL,
  `langdesc_id` varchar(5) NOT NULL,
PRIMARY KEY (`fooddes_id`, `langdesc_id`),
CONSTRAINT FOREIGN KEY (`langdesc_id`) REFERENCES `langdesc` (`id`)  ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FOREIGN KEY (`fooddes_id`) REFERENCES `fooddes` (`id`)  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `nutdata` (
  `fooddes_id` varchar(5) NOT NULL,
  `nutrdef_id` varchar(3) NOT NULL,
  `nutr_val` float(10,3) NOT NULL,
  `num_data_pts` int(5) NOT NULL,
  `std_error` float(8,3) DEFAULT NULL,
  `src_id` varchar(2) NOT NULL,
  `deriv_id` varchar(4) DEFAULT NULL,
  `fooddes_id1` varchar(5) DEFAULT NULL,
  `add_nutr_mark` varchar(1) DEFAULT NULL,
  `num_studies` int(2) DEFAULT NULL,
  `min` float(10,3) DEFAULT NULL,
  `max` float(10,3) DEFAULT NULL,
  `df` float DEFAULT NULL,
  `low_eb` float(10,3) DEFAULT NULL,
  `up_eb` float(10,3) DEFAULT NULL,
  `stat_cmt` varchar(10) DEFAULT NULL,
  `addmod_date` varchar(10) DEFAULT NULL,
  `col` varchar(5) DEFAULT NULL,
PRIMARY KEY (`fooddes_id`, `nutrdef_id`),
CONSTRAINT FOREIGN KEY (`deriv_id`) REFERENCES `deriv` (`id`)  ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FOREIGN KEY (`fooddes_id`) REFERENCES `fooddes` (`id`)  ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FOREIGN KEY (`fooddes_id1`) REFERENCES `fooddes` (`id`)  ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FOREIGN KEY (`nutrdef_id`) REFERENCES `nutrdef` (`id`)  ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FOREIGN KEY (`src_id`) REFERENCES `src` (`id`)  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
